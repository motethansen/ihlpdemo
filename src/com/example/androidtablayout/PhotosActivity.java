package com.example.androidtablayout;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.regex.Pattern;

import static android.content.Intent.*;
import static android.net.Uri.*;

public class PhotosActivity extends Activity {

    // Initializing variables
    EditText inputQRType;
    EditText inputDeviceReference;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photos_layout);

        inputQRType = (EditText) findViewById(R.id.description);
        inputDeviceReference = (EditText) findViewById(R.id.deviceReference);
        Button btnQRscan = (Button) findViewById(R.id.btnQRscan);


    //Listening to button event
        btnQRscan.setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            intent.setPackage("com.google.zxing.client.android");
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            startActivityForResult(intent, 0);
        }

        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");
                String format = data.getStringExtra("SCAN_RESULT_FORMAT");
                // Handle successful scan
                //String result = intent.getExtras().getString(contents);
                inputDeviceReference.setText(contents);
                inputQRType.setText(format);
                //parse(contents);

                //startActivityForResult('android.intent.action.VIEW', content);
                String url;
                url = contents;
                Intent i;
                i = new Intent(ACTION_VIEW, Uri.parse(url));
                startActivity(i);

            } else if (resultCode == RESULT_CANCELED) {
                // Handle cancel
            }
        }
    }
}
