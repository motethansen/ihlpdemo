/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.androidtablayout;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static final int titlebackgroundcolor=0x7f040000;
        public static final int titletextcolor=0x7f040001;
    }
    public static final class drawable {
        public static final int icon=0x7f020000;
        public static final int icon_photos_tab=0x7f020001;
        public static final int icon_songs_tab=0x7f020002;
        public static final int icon_videos_tab=0x7f020003;
        public static final int incidenticon=0x7f020004;
        public static final int photos_gray=0x7f020005;
        public static final int photos_white=0x7f020006;
        public static final int prntoner=0x7f020007;
        public static final int serviceicon=0x7f020008;
        public static final int songs_gray=0x7f020009;
        public static final int songs_white=0x7f02000a;
        public static final int videos_gray=0x7f02000b;
        public static final int videos_white=0x7f02000c;
    }
    public static final class id {
        public static final int TextView04=0x7f07000f;
        public static final int TextView05=0x7f070010;
        public static final int TextView06=0x7f070011;
        public static final int TextView07=0x7f070013;
        public static final int TextView08=0x7f070014;
        public static final int TextView09=0x7f070015;
        public static final int TextView10=0x7f070017;
        public static final int TextView11=0x7f070018;
        public static final int TextView12=0x7f070019;
        public static final int TextView13=0x7f07001b;
        public static final int TextView14=0x7f07001c;
        public static final int TextView15=0x7f07001d;
        public static final int btnNextScreen=0x7f070009;
        public static final int btnQRscan=0x7f070008;
        public static final int buttonsearch=0x7f07000a;
        public static final int description=0x7f070006;
        public static final int deviceReference=0x7f070007;
        public static final int faqImage=0x7f07001e;
        public static final int issueTitle=0x7f070005;
        public static final int label=0x7f070004;
        public static final int myTitle=0x7f070000;
        public static final int radio_left1=0x7f070003;
        public static final int radio_right1=0x7f070002;
        public static final int radiogroup=0x7f070001;
        public static final int tableLayout1=0x7f07000b;
        public static final int tableRow1=0x7f07000c;
        public static final int tableRow2=0x7f07000e;
        public static final int tableRow3=0x7f070012;
        public static final int tableRow4=0x7f070016;
        public static final int tableRow5=0x7f07001a;
        public static final int textView9=0x7f07000d;
    }
    public static final class layout {
        public static final int main=0x7f030000;
        public static final int mytitle=0x7f030001;
        public static final int photos_layout=0x7f030002;
        public static final int songs_layout=0x7f030003;
        public static final int videos_layout=0x7f030004;
    }
    public static final class string {
        public static final int app_name=0x7f050001;
        public static final int hello=0x7f050000;
    }
    public static final class style {
        public static final int WindowTitleBackground=0x7f060000;
        public static final int customTheme=0x7f060001;
    }
}
